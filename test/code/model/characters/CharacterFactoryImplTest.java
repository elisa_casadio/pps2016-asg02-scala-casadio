package code.model.characters;

import code.model.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Elisa on 16/03/2017.
 */
public class CharacterFactoryImplTest {

    private static final int POSITION_X = 10;
    private static final int POSITION_Y = 10;

    private GameCharacter characterActual;
    private GameCharacter characterExpected;

    @Test
    public void createMario() {
        this.characterActual = new MarioCharacter(POSITION_X, POSITION_Y);
        this.characterExpected = GameCharacterFactory.apply("Mario", POSITION_X, POSITION_Y);
        assertEquals(listOfFields(this.characterActual), listOfFields(this.characterExpected));
    }

    private List<Integer> listOfFields(GameCharacter character) {
        List<Integer> list = new ArrayList<>();
        list.add(character.x());
        list.add(character.y());
        list.add(character.height());
        list.add(character.width());
        return list;
    }

    @Test
    public void createMushroom() {
        this.characterActual = new MushroomCharacter(POSITION_X, POSITION_Y);
        this.characterExpected = GameCharacterFactory.apply("Mushroom", POSITION_X, POSITION_Y);
        assertEquals(listOfFields(this.characterActual), listOfFields(this.characterExpected));
    }

    @Test
    public void createTurtle() {
        this.characterActual = new TurtleCharacter(POSITION_X, POSITION_Y);
        this.characterExpected = GameCharacterFactory.apply("Turtle", POSITION_X, POSITION_Y);
        assertEquals(listOfFields(this.characterActual), listOfFields(this.characterExpected));
    }

}