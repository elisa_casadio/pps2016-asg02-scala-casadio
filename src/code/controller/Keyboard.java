package code.controller;

import code.model.MarioCharacter;
import code.view.Platform;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    @Override
    public void keyPressed(KeyEvent e) {

        if (Platform.getPlatform().getMario().characterIsAlive()) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                // per non fare muovere il castello e start
                if (Platform.getPlatform().getxPos() == -1) {
                    Platform.getPlatform().setxPos(0);
                    Platform.getPlatform().setBackground1PosX(-50);
                    Platform.getPlatform().setBackground2PosX(750);
                }
                Platform.getPlatform().getMario().moving_$eq(true);
                Platform.getPlatform().getMario().turnedToRight_$eq(true);
                Platform.getPlatform().setMov(1); // si muove verso sinistra
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (Platform.getPlatform().getxPos() == 4601) {
                    Platform.getPlatform().setxPos(4600);
                    Platform.getPlatform().setBackground1PosX(-50);
                    Platform.getPlatform().setBackground2PosX(750);
                }

                Platform.getPlatform().getMario().moving_$eq(true);
                Platform.getPlatform().getMario().turnedToRight_$eq(false);
                Platform.getPlatform().setMov(-1); // si muove verso destra
            }
            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP && Platform.getPlatform().getMario() instanceof MarioCharacter) {
                ((MarioCharacter)Platform.getPlatform().getMario()).jumping_$eq(true);
                Audio.playSound("/resources/audio/jump.wav");
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Platform.getPlatform().getMario().moving_$eq(false);
        Platform.getPlatform().setMov(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
