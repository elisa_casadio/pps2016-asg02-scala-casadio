package code.view;

import code.controller.Audio;
import code.controller.Keyboard;
import code.model.*;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.*;

import code.utils.Res;
import code.utils.Utils;

@SuppressWarnings("serial")
public class Platform extends JPanel {

    private static final int MARIO_FREQUENCY = 25;
    private static final int MUSHROOM_FREQUENCY = 45;
    private static final int TURTLE_FREQUENCY = 45;
    private static final int BOB_OMB_FREQUENCY = 50;
    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    private static final int TURTLE_DEAD_OFFSET_Y = 30;
    private static final int BOB_OMB_DEAD_OFFSET_Y = 20;
    private static final int FLAG_X_POS = 4650;
    private static final int CASTLE_X_POS = 4850;
    private static final int FLAG_Y_POS = 115;
    private static final int CASTLE_Y_POS = 145;
    private static final String TUNNEL = "Tunnel";
    private static final String BLOCK = "Block";
    private static final String PIECE = "Piece";
    private static final String BOB_OMB = "BobOmb";
    private static final String MARIO = "Mario";
    private static final String MUSHROOM = "Mushroom";
    private static final String TURTLE = "Turtle";

    private static Platform PLATFORM = null;

    private Image imgBackground1;
    private Image imgBackground2;
    private Image castle;
    private Image start;

    private int background1PosX;
    private int background2PosX;
    private int mov;
    private int xPos;
    private int floorOffsetY;
    private int heightLimit;

    private GameCharacter bobOmb;
    private GameCharacter mario;
    private GameCharacter mushroom;
    private GameCharacter turtle;

    private Image imgFlag;
    private Image imgCastle;

    private ArrayList<GameObject> objects;
    private ArrayList<GameObject> pieces;

    private Platform() {
        super();
        this.background1PosX = -50;
        this.background2PosX = 750;
        this.mov = 0;
        this.xPos = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;
        this.imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);

        this.bobOmb = GameCharacterFactory.apply(BOB_OMB, 4000, 258);
        this.mario = GameCharacterFactory.apply(MARIO, 300, 245);
        this.mushroom = GameCharacterFactory.apply(MUSHROOM, 800, 263);
        this.turtle = GameCharacterFactory.apply(TURTLE, 950, 243);

        this.imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imgFlag = Utils.getImage(Res.IMG_FLAG);

        this.objects = new ArrayList<>();

        this.objects.add(GameObjectFactory.apply(TUNNEL, 600, 230));
        this.objects.add(GameObjectFactory.apply(TUNNEL,1000, 230));
        this.objects.add(GameObjectFactory.apply(TUNNEL,1600, 230));
        this.objects.add(GameObjectFactory.apply(TUNNEL,1900, 230));
        this.objects.add(GameObjectFactory.apply(TUNNEL,2500, 230));
        this.objects.add(GameObjectFactory.apply(TUNNEL,3000, 230));
        this.objects.add(GameObjectFactory.apply(TUNNEL,3800, 230));
        this.objects.add(GameObjectFactory.apply(TUNNEL,4500, 230));

        this.objects.add(GameObjectFactory.apply(BLOCK,400, 180));
        this.objects.add(GameObjectFactory.apply(BLOCK,1200, 180));
        this.objects.add(GameObjectFactory.apply(BLOCK,1270, 170));
        this.objects.add(GameObjectFactory.apply(BLOCK,1340, 160));
        this.objects.add(GameObjectFactory.apply(BLOCK,2000, 180));
        this.objects.add(GameObjectFactory.apply(BLOCK,2600, 160));
        this.objects.add(GameObjectFactory.apply(BLOCK,2650, 180));
        this.objects.add(GameObjectFactory.apply(BLOCK,3500, 160));
        this.objects.add(GameObjectFactory.apply(BLOCK,3550, 140));
        this.objects.add(GameObjectFactory.apply(BLOCK,4000, 170));
        this.objects.add(GameObjectFactory.apply(BLOCK,4200, 200));
        this.objects.add(GameObjectFactory.apply(BLOCK,4300, 210));

        this.pieces = new ArrayList<>();
        this.pieces.add(GameObjectFactory.apply(PIECE,402, 145));
        this.pieces.add(GameObjectFactory.apply(PIECE,1202, 140));
        this.pieces.add(GameObjectFactory.apply(PIECE,1272, 95));
        this.pieces.add(GameObjectFactory.apply(PIECE,1342, 40));
        this.pieces.add(GameObjectFactory.apply(PIECE,1650, 145));
        this.pieces.add(GameObjectFactory.apply(PIECE,2650, 145));
        this.pieces.add(GameObjectFactory.apply(PIECE,3000, 135));
        this.pieces.add(GameObjectFactory.apply(PIECE,3400, 125));
        this.pieces.add(GameObjectFactory.apply(PIECE,4200, 145));
        this.pieces.add(GameObjectFactory.apply(PIECE,4600, 40));

        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard());
    }

    public static Platform getPlatform() {
        if (PLATFORM == null) {
            synchronized (Platform.class) {
                if (PLATFORM == null) {
                    PLATFORM = new Platform();
                }
            }
        }
        return PLATFORM;
    }

    public GameCharacter getMario() {
        return this.mario;
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMov() {
        return mov;
    }

    public int getxPos() {
        return xPos;
    }

    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public void setMov(int mov) {
        this.mov = mov;
    }

    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    public void updateBackgroundOnMovement() {
        if (this.xPos >= 0 && this.xPos <= 4600) {
            this.xPos = this.xPos + this.mov;
            // Moving the screen to give the impression that Mario is walking
            this.background1PosX = this.background1PosX - this.mov;
            this.background2PosX = this.background2PosX - this.mov;
        }

        if(this.xPos == 4600) {
            JLabel label = new JLabel(new ImageIcon(Utils.getImage(Res.IMG_WIN)));
            JPanel panel = new JPanel();
            panel.add(label);
            FrameSingleton.getFrame().getContentPane().add(panel);
            FrameSingleton.getFrame().setVisible(true);
        }

        // Flipping between background1 and background2
        if (this.background1PosX == -800) {
            this.background1PosX = 800;
        }
        else if (this.background2PosX == -800) {
            this.background2PosX = 800;
        }
        else if (this.background1PosX == 800) {
            this.background1PosX = -800;
        }
        else if (this.background2PosX == 800) {
            this.background2PosX = -800;
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = (Graphics2D) g;

        for (int i = 0; i < objects.size(); i++) {
            if (this.mario instanceof MarioCharacter && this.mario.isNearbyObject(this.objects.get(i)))
                ((MarioCharacter)this.mario).contactObject(this.objects.get(i));

            if (this.mushroom instanceof MushroomCharacter && this.mushroom.isNearbyObject(this.objects.get(i)))
                ((MushroomCharacter)this.mushroom).contactObject(this.objects.get(i));

            if (this.turtle instanceof TurtleCharacter && this.turtle.isNearbyObject(this.objects.get(i)))
                ((TurtleCharacter)this.turtle).contactObject(this.objects.get(i));

            if (this.bobOmb instanceof BobOmbCharacter && this.bobOmb.isNearbyObject(this.objects.get(i))) {
                ((BobOmbCharacter)this.bobOmb).contactObject(this.objects.get(i));
            }
        }

        for (int i = 0; i < pieces.size(); i++) {
            if (this.mario instanceof MarioCharacter) {
                if (((MarioCharacter)this.mario).contactPiece(this.pieces.get(i))) {
                    Audio.playSound(Res.AUDIO_MONEY);
                    this.pieces.remove(i);
                }
            }
        }


        if (this.mushroom instanceof MushroomCharacter) {
            if (this.turtle instanceof TurtleCharacter && this.turtle.isNearbyCharacter(this.mushroom)) {
                ((TurtleCharacter)this.turtle).contactCharacter(this.mushroom);
            }
            if (this.mario instanceof MarioCharacter && this.mario.isNearbyCharacter(this.mushroom)) {
                ((MarioCharacter) this.mario).contactCharacter(this.mushroom);
            }
            if (this.bobOmb instanceof BobOmbCharacter && this.bobOmb.isNearbyCharacter(this.mushroom)) {
                ((BobOmbCharacter)this.bobOmb).contactCharacter(this.mushroom);
            }
        }
        if (this.turtle instanceof TurtleCharacter) {
            if (this.mushroom instanceof MushroomCharacter && this.mushroom.isNearbyCharacter(this.turtle)) {
                ((MushroomCharacter) this.mushroom).contactCharacter(this.turtle);
            }
            if (this.mario instanceof MarioCharacter && this.mario.isNearbyCharacter(this.turtle)) {
                ((MarioCharacter) this.mario).contactCharacter(this.turtle);
            }
            if (this.bobOmb instanceof BobOmbCharacter && this.bobOmb.isNearbyCharacter(this.turtle)) {
                ((BobOmbCharacter)this.bobOmb).contactCharacter(this.turtle);
            }
        }
        if (this.bobOmb instanceof BobOmbCharacter) {
            if (this.mushroom instanceof MushroomCharacter && this.mushroom.isNearbyCharacter(this.bobOmb)) {
                ((MushroomCharacter) this.mushroom).contactCharacter(this.bobOmb);
            }
            if (this.mario instanceof MarioCharacter && this.mario.isNearbyCharacter(this.bobOmb)) {
                ((MarioCharacter)this.mario).contactCharacter(this.bobOmb);
            }
            if (this.turtle instanceof TurtleCharacter && this.turtle.isNearbyCharacter(this.bobOmb)) {
                ((TurtleCharacter)this.turtle).contactCharacter(this.bobOmb);
            }
        }

        // Moving fixed objects
        this.updateBackgroundOnMovement();
        if (this.xPos >= 0 && this.xPos <= 4600) {
            for (int i = 0; i < objects.size(); i++) {
                objects.get(i).move();
            }

            for (int i = 0; i < pieces.size(); i++) {
                this.pieces.get(i).move();
            }

            if (this.mushroom instanceof MushroomCharacter){
                ((MushroomCharacter)this.mushroom).move();
            }
            if (this.turtle instanceof TurtleCharacter) {
                ((TurtleCharacter)this.turtle).move();
            }
            if (this.bobOmb instanceof BobOmbCharacter) {
                ((BobOmbCharacter)this.bobOmb).move();
            }
        }

        g2.drawImage(this.imgBackground1, this.background1PosX, 0, null);
        g2.drawImage(this.imgBackground2, this.background2PosX, 0, null);
        g2.drawImage(this.castle, 10 - this.xPos, 95, null);
        g2.drawImage(this.start, 220 - this.xPos, 234, null);

        for (int i = 0; i < objects.size(); i++) {
            g2.drawImage(this.objects.get(i).imageObject(), this.objects.get(i).x(),
                    this.objects.get(i).y(), null);
        }

        for (int i = 0; i < pieces.size(); i++) {
            if (this.pieces.get(i) instanceof Piece) {
                g2.drawImage(((Piece) this.pieces.get(i)).imageOnMovement(), this.pieces.get(i).x(),
                        this.pieces.get(i).y(), null);
            }
        }

        g2.drawImage(this.imgFlag, FLAG_X_POS - this.xPos, FLAG_Y_POS, null);
        g2.drawImage(this.imgCastle, CASTLE_X_POS - this.xPos, CASTLE_Y_POS, null);

        if (this.mario instanceof MarioCharacter){
            if (((MarioCharacter)this.mario).jumping())
                g2.drawImage(((MarioCharacter)this.mario).doJump(), this.mario.x(), this.mario.y(), null);
            else
                g2.drawImage(this.mario.walk(Res.IMGP_CHARACTER_MARIO, MARIO_FREQUENCY), this.mario.x(), this.mario.y(), null);
        }

        if (this.mushroom.characterIsAlive()) {
            g2.drawImage(this.mushroom.walk(Res.IMGP_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY), this.mushroom.x(), this.mushroom.y(), null);
        } else if (this.mushroom instanceof MushroomCharacter) {
            g2.drawImage(((MushroomCharacter)this.mushroom).deadImage(), this.mushroom.x(), this.mushroom.y() + MUSHROOM_DEAD_OFFSET_Y, null);
        }

        if (this.turtle.characterIsAlive()) {
            g2.drawImage(this.turtle.walk(Res.IMGP_CHARACTER_TURTLE, TURTLE_FREQUENCY), this.turtle.x(), this.turtle.y(), null);
        } else if (this.turtle instanceof TurtleCharacter) {
            g2.drawImage(((TurtleCharacter)this.turtle).deadImage(), this.turtle.x(), this.turtle.y() + TURTLE_DEAD_OFFSET_Y, null);
        }

        if (this.bobOmb.characterIsAlive()) {
            g2.drawImage(this.bobOmb.walk(Res.IMGP_CHARACTER_BOB_OMB, BOB_OMB_FREQUENCY), this.bobOmb.x(), this.bobOmb.y(), null);
        } else if (this.bobOmb instanceof BobOmbCharacter) {
            g2.drawImage(((BobOmbCharacter)this.bobOmb).deadImage(), this.bobOmb.x(), this.bobOmb.y() + BOB_OMB_DEAD_OFFSET_Y, null);
        }

    }
}
