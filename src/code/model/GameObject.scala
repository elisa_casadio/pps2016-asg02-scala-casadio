package code.model

import java.awt.Image

import code.utils.{Res, Utils}
import code.view.Platform

/**
  * Created by Elisa Casadio on 06/04/2017.
  */
trait GameObject {
  def width: Int
  def height: Int
  def x: Int
  def y: Int
  def imageObject: Image
  def width_=(objectWidth: Int): Unit
  def height_=(objectHeight: Int): Unit
  def x_=(objectX: Int): Unit
  def y_=(objectY: Int): Unit
  def imageObject_=(imageObject: Image): Unit
  def move(): Unit
}

class GameObjectImpl(private var _x: Int,
                     private var _y: Int,
                     private var _width: Int,
                     private var _height: Int) extends GameObject {

  private[this] var image: Image = _

  override def width: Int = _width

  override def height: Int = _height

  override def x: Int = _x

  override def y: Int = _y

  override def imageObject: Image = image

  override def width_=(objectWidth: Int): Unit = _width = objectWidth

  override def height_=(objectHeight: Int): Unit = _height = objectHeight

  override def x_=(objectX: Int): Unit = _x = objectX

  override def y_=(objectY: Int): Unit = _y = objectY

  override def imageObject_=(imageObject: Image): Unit = image = imageObject

  override def move(): Unit = if (Platform.getPlatform.getxPos() >= 0) _x = _x - Platform.getPlatform.getMov

}

object Block {
  private val width = 30
  private val height = 30
}

class Block(private var _x: Int,
            private var _y: Int) extends GameObjectImpl (_x, _y, Block.width, Block.height) {
  imageObject = Utils getImage Res.IMG_BLOCK
}

object Tunnel {
  private val width = 43
  private val height = 65
}

class Tunnel(private var _x: Int,
             private var _y: Int) extends GameObjectImpl (_x, _y, Tunnel.width, Tunnel.height) {
  imageObject = Utils getImage Res.IMG_TUNNEL
}

object Piece {
  private val width = 30
  private val height = 30
  private val pause = 10
  private val flipFrequency = 100
}

class Piece(private var _x: Int,
            private var _y: Int) extends GameObjectImpl (_x, _y, Piece.width, Piece.height) with Runnable {

  private[this] var counter: Int = 0

  imageObject = Utils getImage Res.IMG_PIECE1

  def imageOnMovement: Image = {
    counter = counter + 1
    if (counter % Piece.flipFrequency == 0) Utils getImage Res.IMG_PIECE1 else Utils getImage Res.IMG_PIECE2
  }

  override def run(): Unit = {
    while (true) {
      imageOnMovement
      Thread.sleep(Piece.pause)
    }
  }
}

object GameObjectFactory {
  def apply(typeObject: String, x: Int, y: Int): GameObject = typeObject match {
    case "Block" => new Block(x, y)
    case "Tunnel" => new Tunnel(x, y)
    case _ => new Piece(x, y)
  }
}