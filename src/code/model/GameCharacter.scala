package code.model

import java.awt.Image

import code.utils.{Res, Utils}
import code.view.Platform

/**
  * Created by Elisa Casadio on 08/04/2017.
  */
trait GameCharacter {
  def x: Int
  def y: Int
  def width: Int
  def height: Int
  def counter: Int
  def characterIsAlive: Boolean
  def moving: Boolean
  def turnedToRight: Boolean
  def characterIsAlive_=(isAlive: Boolean): Unit
  def x_=(x: Int): Unit
  def y_=(y: Int): Unit
  def moving_=(moving: Boolean): Unit
  def turnedToRight_=(isToRight: Boolean): Unit
  def counter_=(counter: Int): Unit
  def walk(name: String, frequency: Int): Image
  def isHitAheadObject(gameObject: GameObject): Boolean
  def isHitAheadCharacter(character: GameCharacter): Boolean
  def isHitBackObject(gameObject: GameObject): Boolean
  def isHitBackCharacter(character: GameCharacter): Boolean
  def isHitBelowObject(gameObject: GameObject): Boolean
  def isHitBelowCharacter(character: GameCharacter): Boolean
  def isHitAboveObject(gameObject: GameObject): Boolean
  def isNearbyCharacter(character: GameCharacter): Boolean
  def isNearbyObject(gameObject: GameObject): Boolean
}

private object GameCharacterImpl {
  private val proximity_margin: Int = 10
  private val proximity_margin_hit: Int = 5
}

class GameCharacterImpl (private[this] var _x: Int,
                         private[this] var _y: Int,
                         private[this] var _width: Int,
                         private[this] var _height: Int) extends GameCharacter {

  private[this] var count: Int = 0
  private[this] var alive: Boolean = true
  private[this] var move: Boolean = false
  private[this] var toRight: Boolean = true

  override def x: Int = _x

  override def y: Int = _y

  override def width: Int = _width

  override def height: Int = _height

  override def counter: Int = count

  override def characterIsAlive: Boolean = alive

  override def moving: Boolean = move

  override def turnedToRight: Boolean = toRight

  override def characterIsAlive_=(isAlive: Boolean): Unit = alive = isAlive

  override def x_=(x: Int): Unit = _x = x

  override def y_=(y: Int): Unit = _y = y

  override def moving_=(moving: Boolean): Unit = move = moving

  override def turnedToRight_=(isToRight: Boolean): Unit = toRight = isToRight

  override def counter_=(counter: Int): Unit = count = counter

  override def walk(name: String, frequency: Int): Image = {
    count = count + 1
    val status: String = if (!move || count % frequency == 0) Res.IMGP_STATUS_ACTIVE else Res.IMGP_STATUS_NORMAL
    val direction: String = if (toRight) Res.IMGP_DIRECTION_DX else Res.IMGP_DIRECTION_SX
    Utils getImage Res.IMG_BASE + name + status + direction + Res.IMG_EXT
  }

  import GameCharacterImpl._

  override def isHitAheadObject(gameObject: GameObject): Boolean = !(_x + _width < gameObject.x || _x + _width > gameObject.x + proximity_margin_hit || _y + _height <= gameObject.y || _y >= gameObject.y + gameObject.height)

  override def isHitAheadCharacter(character: GameCharacter): Boolean = toRight && !(_x + _width < character.x || _x + _width > character.x + proximity_margin_hit || _y + _height <= character.y || _y >= character.y + character.height)

  override def isHitBackObject(gameObject: GameObject): Boolean = !(_x > gameObject.x + gameObject.width || _x + _width < gameObject.x + gameObject.width || _y + _height <= gameObject.y || y >= gameObject.y + gameObject.height)

  override def isHitBackCharacter(character: GameCharacter): Boolean = !(_x > character.x + character.width || _x + _width < character.x + character.width - proximity_margin_hit || _y + _height <= character.y || _y >= character.y + character.height)

  override def isHitBelowObject(gameObject: GameObject): Boolean = !(_x + _width < gameObject.x + proximity_margin_hit || _x > gameObject.x + gameObject.width - proximity_margin_hit || _y + _height < gameObject.y || _y + _height > gameObject.y + proximity_margin_hit)

  override def isHitBelowCharacter(character: GameCharacter): Boolean = !(_x + _width < character.x || _x > character.x + character.width || _y + _height < character.y || _y + _height > character.y)

  override def isHitAboveObject(gameObject: GameObject): Boolean = !(_x + _width < gameObject.x + proximity_margin_hit || _x > gameObject.x + gameObject.width - proximity_margin_hit || _y < gameObject.y + gameObject.height || _y > gameObject.y + gameObject.height + proximity_margin_hit)

  override def isNearbyCharacter(character: GameCharacter): Boolean = (_x > character.x - proximity_margin && _x < character.x + character.width + proximity_margin) || (_x + _width > character.x - proximity_margin && _x + _width < character.x + character.width + proximity_margin)

  override def isNearbyObject(gameObject: GameObject): Boolean = (_x > gameObject.x - proximity_margin && _x < gameObject.x + gameObject.width + proximity_margin) || (_x + _width > gameObject.x - proximity_margin && _x + _width < gameObject.x + gameObject.width + proximity_margin)

}

trait GameCharacterStrategy {
  def move(): Unit
  def contactCharacter(character: GameCharacter): Unit
  def contactObject(gameObject: GameObject): Unit
}

private object MarioCharacter {
  private val marioOffsetYInitial: Int = 243
  private val floorOffsetYInitial: Int = 293
  private val width: Int = 28
  private val height: Int = 50
  private val jumpingLimit: Int = 42
}

class MarioCharacter (private[this] var _x: Int,
                      private[this] var _y: Int) extends GameCharacterImpl(_x, _y, MarioCharacter.width, MarioCharacter.height) with GameCharacterStrategy {

  import MarioCharacter._

  private[this] var jump: Boolean = false
  private[this] var jumpExtent: Int = 0

  Utils getImage Res.IMG_MARIO_DEFAULT

  def jumping: Boolean = jump

  def jumping_=(jumping: Boolean): Unit = jump = jumping

  def doJump(): Image = {
    var correctNameResource: String = ""
    jumpExtent = jumpExtent + 1
    import ImplementationHelper._
    if (jumpExtent < jumpingLimit) {
        if (y > Platform.getPlatform.getHeightLimit) y = y - 4 else jumpExtent = MarioCharacter.jumpingLimit
        correctNameResource = getCorrectNameResource(true)
    } else {
      if (y + height < Platform.getPlatform.getFloorOffsetY) {
        y = y + 1
        correctNameResource = getCorrectNameResource(true)
      } else {
        correctNameResource = getCorrectNameResource(false)
        jump = false
        jumpExtent = 0
      }
    }
    Utils getImage correctNameResource
  }

  private[this] object ImplementationHelper {
    def getCorrectNameResource(jump: Boolean): String = {
      if (jump) {
        if (turnedToRight) Res.IMG_MARIO_SUPER_DX else Res.IMG_MARIO_SUPER_SX
      } else {
        if (turnedToRight) Res.IMG_MARIO_ACTIVE_DX else Res.IMG_MARIO_ACTIVE_SX
      }
    }
  }

  def contactPiece(piece: GameObject): Boolean = isHitBackObject(piece) || isHitAboveObject(piece) || isHitAheadObject(piece) || isHitBelowObject(piece)

  override def move(): Unit = if (Platform.getPlatform.getxPos() >= 0) x = x - Platform.getPlatform.getMov

  override def contactCharacter(character: GameCharacter): Unit = {
    if (isHitAheadCharacter(character) || isHitBackCharacter(character)) {
      if (character.characterIsAlive) { moving = false; characterIsAlive = false } else characterIsAlive_=(true)
    } else {
      if (isHitBelowCharacter(character)) {character.moving = false; character.characterIsAlive = false}
    }
  }

  override def contactObject(gameObject: GameObject): Unit = {
    if (isHitAheadObject(gameObject) && turnedToRight || isHitBackObject(gameObject) && !turnedToRight) {
      Platform.getPlatform.setMov(0)
      moving = false
    }
    if (isHitBelowObject(gameObject) && jump) Platform.getPlatform.setFloorOffsetY(gameObject.y) else if (!isHitBelowObject(gameObject)) {
      Platform.getPlatform.setFloorOffsetY(floorOffsetYInitial)
      if (!jump) y = marioOffsetYInitial
      if (isHitAboveObject(gameObject)) Platform.getPlatform.setHeightLimit(gameObject.y + gameObject.height) else if (!isHitAboveObject(gameObject) && !jump) Platform.getPlatform.setHeightLimit(0)
    }
  }

}

private object EnemyCharacter {
  private val pause: Int = 15
}

abstract class EnemyCharacter (private[this] var _x: Int,
                               private[this] var _y: Int,
                               private[this] var _width: Int,
                               private[this] var _height: Int) extends GameCharacterImpl (_x, _y, _width, _height) with GameCharacterStrategy with Runnable {

  private[this] var offsetX: Int = 1

  turnedToRight = true
  moving = true
  private[this] val threadEnemy = new Thread(this)
  threadEnemy start()

  override def move(): Unit = {
    offsetX = if (turnedToRight) 1 else -1
    x = x + offsetX
  }

  import ImplementationHelper._

  override def contactCharacter(character: GameCharacter): Unit = if (isHitAheadCharacter(character) && turnedToRight) setChangeEnemy(willBeRight = false, -1) else if (isHitBackCharacter(character) && !turnedToRight) setChangeEnemy(willBeRight = true, 1)

  private[this] object ImplementationHelper {
    def setChangeEnemy(willBeRight: Boolean, offset: Int): Unit = { turnedToRight = willBeRight; offsetX = offset }
  }

  override def contactObject(gameObject: GameObject): Unit = {
    if (isHitAheadObject(gameObject) && turnedToRight) setChangeEnemy(willBeRight = false, -1) else if (isHitBackObject(gameObject) && !turnedToRight) {
      turnedToRight = true
      setChangeEnemy(willBeRight = true, 1)
    }
  }

  override def run(): Unit = {
    while (characterIsAlive) {
      move()
      Thread.sleep(EnemyCharacter.pause)
    }
  }

  def deadImage(): Image

}

private object MushroomCharacter {
  private val width: Int = 27
  private val height: Int = 30
}

class MushroomCharacter (private var _x: Int,
                         private var _y: Int) extends EnemyCharacter (_x, _y, MushroomCharacter.width, MushroomCharacter.height) {

  private[this] val image: Image = Utils getImage Res.IMG_MUSHROOM_DEFAULT

  def imageMushroom: Image = image

  override def deadImage(): Image = {
    val imageDead: String = if (turnedToRight) Res.IMG_MUSHROOM_DEAD_DX else Res.IMG_MUSHROOM_DEAD_SX
    Utils getImage imageDead
  }

}

private object TurtleCharacter {
  private val width: Int = 43
  private val height: Int = 50
}

class TurtleCharacter (private var _x: Int,
                       private var _y: Int) extends EnemyCharacter (_x, _y, TurtleCharacter.width, TurtleCharacter.height) {

  private[this] val image: Image = Utils getImage Res.IMG_TURTLE_IDLE

  def imageTurtle: Image = image

  override def deadImage(): Image = Utils getImage Res.IMG_TURTLE_DEAD

}

private object BobOmbCharacter {
  private val width: Int = 36
  private val height: Int = 38
}

class BobOmbCharacter(private var _x: Int,
                      private var _y: Int) extends EnemyCharacter (_x, _y, BobOmbCharacter.width, BobOmbCharacter.height) {

  private[this] val image: Image = Utils getImage Res.IMG_BOB_OMB_DX

  def imageBobOmb: Image = image

  override def deadImage(): Image = {
    val imageDead: String = if (turnedToRight) Res.IMG_BOB_OMB_DEAD_DX else Res.IMG_BOB_OMB_DEAD_SX
    Utils getImage imageDead
  }

}

object GameCharacterFactory {
  def apply(typeObject: String, x: Int, y: Int): GameCharacter = typeObject match {
    case "BobOmb" => new BobOmbCharacter(x, y)
    case "Mario" => new MarioCharacter(x, y)
    case "Mushroom" => new MushroomCharacter(x, y)
    case _ => new TurtleCharacter(x, y)
  }
}